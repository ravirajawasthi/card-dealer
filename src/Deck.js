import React, { Component } from 'react'
import Axios from 'axios'
import './Deck.css'

class Deck extends Component{
    constructor(props){
        super(props)
        this.state = {
            deck_id: "",
            cards_left: 0,
            cards_img: []
        }
        this.getNewDeck = this.getNewDeck.bind(this);
        this.dealCard = this.dealCard.bind(this);
    }
    componentDidMount(){
        this.getNewDeck()
    }
    getNewDeck(){
        Axios.get("https://deckofcardsapi.com/api/deck/new/shuffle/").then(response => {
            this.setState({
                deck_id: response.data.deck_id,
                cards_left: response.data.remaining
            })
        })
    }
    dealCard(){
        if ( this.state.cards_left === 0 ){return }
        Axios.get(`https://deckofcardsapi.com/api/deck/${this.state.deck_id}/draw/`).then(response => {
            this.setState(st => {
                let card = response.data.cards[0]
                let newImageArray = st.cards_img
                let randomRotation = Math.floor(Math.random() * 60 ) -30;
                let imgStyle = {
                    position: "absolute",
                    zIndex: 52 - this.state.cards_left,
                    left: "42%",
                    marginTop: "50px",
                    transform: `rotate(${randomRotation}deg)`
                }
                newImageArray.push(<img style = {imgStyle} src={card.image} alt = {card.suit + " " + card.value} />)
                return {cards_img: newImageArray, cards_left: response.data.remaining}
            }, ()=> {console.log(this.state)})
        })
    }
    render(){
        return(
            <div className = "Deck">
                <h1>Card dealer</h1>
                <h3>{`Cards remaining : ${this.state.cards_left}`}</h3>
                <button onClick={this.dealCard}>Deal Card</button>
                <div className = {"Deck-cards"}>
                	{this.state.cards_img}
                </div>
            </div>         
        )
    }
}

export default Deck